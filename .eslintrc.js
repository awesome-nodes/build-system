module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/typescript',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: false,
    },
    project: 'tsconfig.json',
  },
  plugins: [
    '@typescript-eslint',
    '@typescript-eslint/tslint',
    'eslint-plugin',
    'import',
    'unicorn',
  ],
  env: {
    es6: true,
    node: true,
  },
  rules: {
    // The '1tbs' style is broken (global, class and function scopes must have curly braces at next line)
    // and 'allman' also does not work because it is impractically having procedural local scope entities
    // indistinguishable from every surrounding scope.
    //'brace-style': ['error', '1tbs', {'allowSingleLine': true}],
    'comma-dangle': ['error', 'only-multiline'],
    curly: ['error', 'multi-or-nest', 'consistent'],
    indent: ['error', 4, {"SwitchCase": 1}],
    'no-fallthrough': 0,
    'max-len': ['error', {'code': 120}],
    'object-curly-newline': ['error', {'consistent': true}],
    'object-curly-spacing': ['error', 'always', {'objectsInObjects': false}],
    'padding-line-between-statements': ['error',
      {
        blankLine: 'always',
        prev: '*',
        next: ['export', 'class', 'function', 'return'],
      },
      {
        blankLine: 'always',
        prev: ['export', 'class', 'function'],
        next: '*',
      },
    ],
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    // unicorn
    'unicorn/catch-error-name': [
      "error",
      {
        "name": "exception"
      }
    ],
    'unicorn/import-index': 0,
    // import
    'import/newline-after-import': 0,
    'import/no-duplicates': 1,
    'import/max-dependencies': [1, {'max': 10}],
    // tslint
    '@typescript-eslint/interface-name-prefix': 0,
    '@typescript-eslint/tslint/config': [1, {
      lintFile: './tslint.json',
    }],
  },
};
