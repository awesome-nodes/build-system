import chalk from 'chalk';
import { OptionValues } from 'commander';
import lib from 'lib/core/lib';
import exit from 'lib/node/exit';
import npm from 'lib/node/npm-config';
import cmdlet from 'lib/npm-install.cmdlet';
import * as path from 'path';
import * as process from 'process';
import * as sh from 'shelljs';
import { ExecOutputReturnValue } from 'shelljs';
import { Nullable } from 'simplytyped';
import * as util from 'util';
import FORCE_GLOBAL = cmdlet.FORCE_GLOBAL;
import HTTP_PROXY = cmdlet.HTTP_PROXY;
import HTTPS_PROXY = cmdlet.HTTPS_PROXY;
import NPM_CACHE = cmdlet.NPM_CACHE;
import NPM_REGISTRY = cmdlet.NPM_REGISTRY;
import npmInstallCommandLet = cmdlet.npmInstallCommandLet;
import options = cmdlet.options;
import STRICT_SSL = cmdlet.STRICT_SSL;
import USE_PROXY = cmdlet.USE_PROXY;
import ExitCodes = exit.ExitCodes;
import RegisterExitHandler = exit.RegisterExitHandler;
import SignalTypes = exit.SignalTypes;
import exec = lib.exec;
import logError = lib.logError;
import log = lib.logMessage;
import logType = lib.logType;
import ProxyConfig = npm.ProxyConfig;

let npmProxySettings: Nullable<ProxyConfig>;

/**
 * Main Entry Point
 */
export default (programOptions: OptionValues) =>
{
    if (programOptions.help || options.help) {
        log(npmInstallCommandLet.usage);
        process.exit(0);
    }

    // Use proxy settings from host as default.
    npmProxySettings = ProxyConfig.Default.clone();

    if (USE_PROXY)
        // Make the command-let gracefully respond to shutdown signals
        RegisterExitHandler((exitCode: Nullable<number>, signal: Nullable<string>) =>
        {
            switch (signal) {
                // Process terminal closed
                case SignalTypes[SignalTypes.SIGHUP]:
                // User exit through ctrl-C
                case SignalTypes[SignalTypes.SIGINT]:
                // Application shut down
                case SignalTypes[SignalTypes.SIGQUIT]:
                // Application exit
                case SignalTypes[SignalTypes.SIGABRT]:
                // User requests termination
                case SignalTypes[SignalTypes.SIGTERM]:
                case null:
                    // exit gracefully and revert npm settings used during npm install
                    ResetNPMProxy();
                    break;
                default:
                    // Node process is in exit state, we have to exit immediately.
                    return true;
            }
            return true;
        });

    try {
        ShowSystemInfo();
        if (USE_PROXY)
            SetNPMProxy();

        lib.exec(`npm config set registry ${NPM_REGISTRY}`);
        InstallGlobalProjectDependencies();
        InstallProjectDependenciesAndExecuteBuild();
    }
    finally {
        if (USE_PROXY)
            ResetNPMProxy();
    }
};

/**
 * Sets npm proxy servers.
 */
function SetNPMProxy()
{
    if (npmProxySettings == null)
        return;

    npmProxySettings.httpProxy  = HTTP_PROXY;
    npmProxySettings.httpsProxy = HTTPS_PROXY;
    npmProxySettings.strictSSL  = STRICT_SSL;
}

/**
 * Resets npm proxy servers to system default (determined at script start).
 */
function ResetNPMProxy()
{
    if (npmProxySettings == null)
        return;

    npmProxySettings.httpProxy  = ProxyConfig.Default.httpProxy;
    npmProxySettings.httpsProxy = ProxyConfig.Default.httpsProxy;
    npmProxySettings.strictSSL  = ProxyConfig.Default.strictSSL;

    npmProxySettings = null;
}

/**
 * Displays all system environment settings this npm-install depends on.
 */
function ShowSystemInfo()
{
    log(`Start of script............: '%s'`, chalk.bold.cyan(path.basename(__filename)));
    log(`Setting for env.http_proxy.: '%s'`, chalk.cyan(process.env.http_proxy as string));
    log(`Setting for env.https_proxy: '%s'`, chalk.cyan(process.env.https_proxy as string));
    log(`Setting for env.no_proxy...: '%s'`, chalk.cyan(process.env.no_proxy as string));
    log(`Setting for npm.proxy......: '%s'`, chalk.cyan((npmProxySettings as ProxyConfig).httpProxy));
    log(`Setting for npm.https-proxy: '%s'`, chalk.cyan((npmProxySettings as ProxyConfig).httpsProxy));
    log(`Setting for npm.strict-ssl : '%s'`, chalk.cyan((npmProxySettings as ProxyConfig).strictSSL));
}

/**
 * Installs a global npm package.
 * @param {string} globalPackage
 * @param {string} version
 */
function InstallGlobalPackage(globalPackage: string, version: string)
{
    // If a special version is given, we will install it.
    if (version)
        globalPackage += `@${version}`;

    log(chalk.bold.cyan('-----------------------------------------------------------'));
    logType(chalk.blue('npm'), `%s Installing '%s' ...`, chalk.bold.cyan('Start:'), chalk.bold.cyan(globalPackage));

    let installCMD = `npm install -g ${globalPackage}`;
    if (NPM_CACHE != npm.Config.get('cache'))
        installCMD += ` --cache "${NPM_CACHE}"`;
    exec(installCMD);

    logType(chalk.green('npm'), `%s Installing '%s' ...`, chalk.bold.cyan('End:'), globalPackage);
}

/**
 * Installs global project dependencies only if they are not existing or having different versions.
 * @param {string} packageJsonFile
 */
function InstallGlobalProjectDependencies(packageJsonFile?: string)
{
    if (!packageJsonFile)
        packageJsonFile = 'package.json';

    logType(chalk.blue('npm'), `Loading global packages from '%s' ...`, chalk.bold.cyan(packageJsonFile));

    // Load global dependency configuration
    const globalDependencies = require(`${process.cwd()}/${packageJsonFile}`).globalDependencies;

    if (!globalDependencies) {
        logError(`Config section 'globalPackages' not found within '%s'. Exiting...`, packageJsonFile);
        sh.exit(ExitCodes.CommandNotFound);
    }

    // Read package list from npm. Look at top level only for excluding possible extraneous packages from BuildSystem.
    const versionString = lib.exec(
        util.format('npm list -g %s --depth=0', Object.keys(globalDependencies).join(' ')),
        undefined,
        (_exec: ExecOutputReturnValue) => (_exec.stderr.indexOf('npm ERR! extraneous:') > -1 || _exec.code == 1),
    ).stdout;

    // Loop over 'globalDependencies' config section from package.json and verify all versions
    for (const _package in globalDependencies) {
        if (!globalDependencies.hasOwnProperty(_package))
            continue;

        const _version = globalDependencies[_package];

        if (!FORCE_GLOBAL) {
            const regexp         = new RegExp(util.format('(%s)\@([\w\d\.-_]+)', _package), 'g');
            const packageVersion = regexp.exec(versionString);

            // Verify version or compare by package name if no version specified.
            if (packageVersion && (!_version || packageVersion[2] === _version) && packageVersion[1] === _package) {
                logType(chalk.green('npm'), `Package '%s' installed, skipping...`, chalk.bold.cyan(_package));
                continue;
            }
        }
        // Install npm package globally
        InstallGlobalPackage(_package, _version);
    }
}

/**
 * Installs all npm package dependencies by using a global npm-cache directory specified by the [cache] option.
 */
function InstallProjectDependenciesAndExecuteBuild()
{
    log(chalk.bold.cyan('-----------------------------------------------------------'));
    logType(chalk.blue('npm'), chalk.bold.cyan('Start:') + chalk.bold.yellow(' Installing project dependencies...'));
    npmInstallCommandLet.exec();
    logType(chalk.green('npm'), chalk.bold.cyan('End:') + chalk.bold.yellow(' Installing project dependencies.'));
}
