import CommandLet from 'lib/core/cmd';
import lib from 'lib/core/lib';

export namespace cmdlet
{
    /**
     * BuildSystemDemo Command-Let
     * Provides the following:
     * - Explains on how to write a awesome command-let with BuildSystem.
     */
    export const buildSystemDemoCommandLet = new class BuildSystemDemoCommandLet extends CommandLet
    {
        public exec()
        {
            //region Prepare Arguments

            let command = `npm i`;
            if (PRODUCTION)
                command += ' -g';

            //endregion

            lib.exec(command);
        }
    }(
        'demo',
        'No chance to escape not gracefully . . .',
        [
            {
                name        : 'production',
                alias       : 'p',
                type        : Boolean,
                description : 'You definitely know what you\'re doing :)',
                defaultValue: false,
            },
            {
                name       : 'help',
                alias      : 'h',
                type       : Boolean,
                description: 'Print this usage guide.',
            },
        ],
    );

    // Initialize command line processor
    export const options = buildSystemDemoCommandLet.args;

    // Read command line parameters
    export const PRODUCTION: boolean = options.production;

}

export default cmdlet;
