import coreUtil from 'lib/core/util';
import path from 'path';

/**
 * The core of BuildSystem. Simply extend with own behavior by replacing through reassignment or overriding with wrapper
 * functions. Plugin extensibility requires that extensions must finally call the previous library function
 * (old function reference).
 * @example
 * import {lib} from 'lib/core/util';
 *
 * // Replace
 * lib.logNameHandler = (type: string) =>
 * {
 *     return `[ACME] [${type}]`
 * };
 *
 * // Extend
 * const oldLogFunction = libUtil.log;
 * lib.log = (task: string, msg: string) =>
 * {
 *     oldLogFunction(task, msg.replace("\r\n","\n"));
 * };
 * Warning: High potential for call stack looping (endless recursive calls). Always extend with care!
 */
namespace lib
{
    //region util

    /** @inheritDoc */
        //tslint:disable-next-line:no-shadowed-variable
    export const util = coreUtil;

    util.setProperty('packageJSON', require(path.resolve('package.json')));
    const pkgName = /((@*[\w-]+)\/+)*([\w-]+)/.exec(coreUtil.packageJSON.name as string);
    util.setProperty('packageScope', pkgName![2]);
    util.setProperty('packageName', pkgName![3]);

    // Publish lib to core util
    util.setProperty('lib', lib);

    /** @inheritDoc */
    export const setProperty = util.setProperty;

    //endregion

    //region Log

    /** @inheritDoc */
    export const log        = util.log;
    /** @inheritDoc */
    export const logFormat  = util.logFormat;
    /** @inheritDoc */
    export const logMessage = util.logMessage;
    /** @inheritDoc */
    export const logType    = util.logType;
    /** @inheritDoc */
    export const logWarning = util.logWarning;
    /** @inheritDoc */
    export const logError   = util.logError;

    //endregion

    //region IO

    /** @inheritDoc */
    export const deleteRecursive = util.deleteRecursive;
    /** @inheritDoc */
    export const replaceInFile   = util.replaceInFile;

    //endregion

    //region Shell

    /** @inheritDoc */
    export const exec = util.exec;

    //endregion

    //region Plugins

    /** @inheritDoc */
    export const logNameHandler: (string: string) => string = util.logFormatHandler;

    //endregion
}

export default lib;
