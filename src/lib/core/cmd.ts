import commandLineArgs from 'command-line-args';
import commandLineUsage from 'command-line-usage';

export type Option = commandLineArgs.OptionDefinition | commandLineUsage.OptionDefinition;

/**
 * Represents a command line configuration for a BuildSystem command library.
 */
export default class CommandLet implements commandLineUsage.Content
{
    constructor(
        public readonly header: string,
        public readonly content: string | string[] | any[] | { data: any; options: any },
        public readonly options: Option[],
        public readonly parseOptions: commandLineArgs.ParseOptions = {
            camelCase: true,
            partial  : true,
        })
    {
    }

    /**
     * Returns an object containing option values parsed from the command line using this command-let configuration.
     * Note: All dash cased command-line arguments are accessible through a camel cased object member.
     * @returns {commandLineArgs.CommandLineOptions}
     */
    public get args(): commandLineArgs.CommandLineOptions
    {
        return commandLineArgs(this.options, this.parseOptions);
    }

    /**
     * Generates a usage guide suitable for this command-let instance.
     * @returns {string}
     */
    public get usage(): string
    {
        return commandLineUsage([
            this, {
                header    : 'Options',
                optionList: this.options,
            },
        ]);
    }
}
