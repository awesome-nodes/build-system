import { bs } from 'bin/bs';
import chalk from 'chalk';
import * as Console from 'console';
import * as fs from 'fs';
import exit from 'lib/node/exit';
import * as rimraf from 'rimraf';
import { Options } from 'rimraf';
import * as sh from 'shelljs';
import { ExecOptions, ExecOutputReturnValue } from 'shelljs';
import { Nullable, ObjectKeys } from 'simplytyped';
import { PackageJson } from 'type-fest';
import * as nodeUtil from 'util';

/**
 * Provides managed runtime utilities and behavior.
 * Acts as a scopeless static function storage for 'this' context interaction.
 * Note: Please extend this object with command line utilities and shortcuts only!
 */
namespace util
{
    export declare const lib: typeof util & (() => void);
    export declare const packageJSON: PackageJson;
    export declare const packageName: string;
    export declare const packageScope: string;

    /**
     * Creates property descriptor for the provided peroperty key using the calling 'this' context as target object.
     * @param {ObjectKeys} propertyKey
     * @param value
     */
    export function setProperty(this: object, propertyKey: ObjectKeys, value: unknown)
    {
        if (Object.getOwnPropertyDescriptor(util, propertyKey))
            throw Error(`Cannot redefine property '${String(propertyKey)}'.`);

        Object.defineProperty(this, propertyKey, {
            get         : () => value,
            enumerable  : true,
            configurable: true,
        });
    }

    //region Logging

    declare type LogMessageType = 'info' | 'warning' | 'error' | string;
    export let logFormatHandler: (name: string) => string;

    /**
     * Returns a formatted string which represents the current log context.
     * The log entry name gets resolved in the following order:
     * - 1st 'this' context (extended object oriented logic)
     * - 2ndt bs messageType name (simple command-lets)
     * - 3rd BuildSystem product name (fallback)
     * @param {'info' | 'warning' | 'error' | string} messageType
     * @returns {string}
     */
    export function logFormat(this: typeof lib & (() => void), messageType: LogMessageType)
    {
        return `[${(this && ('name' in this)) ? this.name
            : (bs
                ? bs.name()
                : packageJSON.name)}] [${messageType}]`;
    }

    /**
     * Creates a log message by using a formatted string for interpolation with optional parameters.
     * @param {string} task
     * @param {string} msg
     * @param {typeof Console.log | typeof Console.error} logFunction
     * @param {...string[]} params
     */
    export function log(
        task: string,
        msg: string,
        logFunction: typeof Console.log | typeof Console.warn | typeof Console.error = console.log,
        ...params: string[])
    {
        if (params.length > 0)
            msg = nodeUtil.format.apply(null, [msg, ...params]);
        logFunction(`${(typeof lib.logFormatHandler == 'function' && lib.logFormatHandler || lib.logFormat)(task)} ${msg}`);
    }

    /**
     * Creates a 'info' log message by using a formatted string for interpolation with optional parameters.
     * @param {string} msg
     * @param {string[]} params
     */
    export function logMessage(msg: string, ...params: string[])
    {
        lib.log(chalk.bold('info'), msg, console.log, ...params);
    }

    /**
     * Creates a specific type log message by using a formatted string for interpolation with optional parameters.
     * @param {string} type
     * @param {string} msg
     * @param {string[]} params
     */
    export function logType(type: LogMessageType, msg: string, ...params: string[])
    {
        lib.log(chalk.bold(type), msg, console.log, ...params);
    }

    /**
     * Creates a 'warning' log message by using a formatted string for interpolation with optional parameters.
     * @param {string} msg
     * @param {string[]} params
     */
    export function logWarning(msg: string, ...params: string[])
    {
        lib.log(chalk.bold.yellow('warning'), msg, console.warn, ...params);
    }

    /**
     * Creates a 'error' log message by using a formatted string for interpolation with optional parameters.
     * @param {string} msg
     * @param {string[]} params
     */
    export function logError(msg: string, ...params: string[])
    {
        lib.log(chalk.bold.redBright('error'), msg, console.error, ...params);
    }

    /**
     * Provides synchronous deep- deletion of files and directories
     * as a faster alternative to the rm -rf shell command.
     * @param {string} path
     * @param {rimraf.Options} options
     */
    export function deleteRecursive(path: string, options: Options = {maxBusyTries: 3})
    {
        if (bs.opts().verbose)
            lib.logType(
                'delete-recursive',
                chalk.bold(`${path}${options ? `, ${JSON.stringify(options)}` : ''}`),
            );

        rimraf.sync(path, options);
    }

    //endregion

    //region File System

    /**
     * Replaces all occurrences of the specified target string within the specified file.
     * @param filePath
     * @param target
     * @param replacement
     * @returns {Promise}
     */
    export function replaceInFile(filePath: string, target: string, replacement: string)
    {
        if (bs.opts().verbose)
            lib.log('replace-in-file', chalk.bold(`${filePath}/${target}/${replacement}`));

        return new Promise((resolve, reject) =>
        {
            fs.readFile(filePath, 'utf8', (err: any, data: string) =>
            {
                if (err) {
                    lib.logError(err);
                    reject(err);
                }

                const result = data.replace(new RegExp(target, 'g'), replacement);

                fs.writeFile(filePath, result, 'utf8', (error: Nullable<NodeJS.ErrnoException>) =>
                {
                    if (error) {
                        lib.logError(String(error));
                        reject(error);
                    }

                    resolve(true);
                });
            });
        });
    }

    /**
     * Executes the provided command synchronously and returns the execution output from shelljs.     *
     * @param {string} command
     * @param {ExecOptions} options
     * @param {(exec: ExecOutputReturnValue) => boolean} errorCallback A callback function for specific error code
     * handling (returns true if no error).
     * @returns {ExecOutputReturnValue}
     */
    export function exec(
        command: string,
        options?: ExecOptions,
        errorCallback?: (exec: ExecOutputReturnValue) => boolean)
    {
        if (bs.opts().verbose) {
            command += ' --verbose';
            lib.log('exec', `cmd:${chalk.bold(`${command}${options ? `, ${JSON.stringify(options)}` : ''}`)}`);
        }

        const result = sh.exec(command, options || {}) as ExecOutputReturnValue;

        let statement: string;
        let exitCode = 0;

        if (result == null)
            statement = chalk.red.bold('<unknown exec result>');
        else if (typeof result === 'object') {
            statement = ('stderr' in result ? result.stderr : result).toString();
            exitCode  = 'code' in result ? result.code : 0;
        } else statement = JSON.stringify(result);

        if (bs.opts().verbose)
            lib.log('exec', `cmd:${command} ${chalk.bold.underline(`finished. Exit Code: '${exitCode}'`)}`);

        // All unexpected behavior is handled by BuildSystem itself.
        // Only exec calls with a proper output result shall be handled by their owners, otherwise throw.
        if (!result && !errorCallback || (exitCode > 0 && (!errorCallback || !errorCallback(result)))) {
            if (statement && statement != 'undefined')
                lib.logError(chalk.bold.red(statement));
            process.exitCode = exit.ExitCodes.ShellBuiltinMisuse;
            throw new Error(`Shell execution of command '${command}' failed. Exit Code: '${exitCode}'`);
        }
        return result;
    }

    //endregion

    //region Utilities

    /**
     * @param error the error object.
     * @returns if given error object is a NodeJS error.
     */
    export function isNodeError(error: unknown): error is NodeJS.ErrnoException
    {
        return error instanceof Error;
    }

    //endregion
}

export default util;
