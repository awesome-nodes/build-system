import chalk from 'chalk';
import lib from 'lib/core/lib';

namespace npm
{
    /**
     * Represents a npm settings base object which provides editing of npm config properties.
     */
    export abstract class Config
    {
        private static action(name: 'get' | 'set' | 'rm', configItem: string)
        {
            lib.logMessage(`${this.name}: ${name} npm.config.${configItem}`);
            const returnValue = lib.exec(`npm config ${name} ${configItem}`, {silent: true});
            return (returnValue !== null && typeof returnValue === 'object' && 'stdout' in returnValue
                    ? returnValue.stdout
                    : `${returnValue}`
            ).trim();
        }

        public static get(configItem: string)
        {
            return this.action('get', configItem);
        }

        public static set(configItem: string, value: string)
        {
            return this.action('set', `${configItem}: ${chalk.bold.cyan(value)}`);
        }

        public static rm(configItem: string)
        {
            return this.action('rm', `${configItem}`);
        }

        public abstract clone(): Config;
    }

    /**
     * Represents default npm proxy settings. All members of this setting object are readonly.
     */
    export class ProxyDefaultConfig extends Config
    {
        private static _Default: npm.ProxyDefaultConfig;

        /**
         * Returns the npm proxy configuration settings of the local nodeJS host.
         * @returns {npm.ProxyDefaultConfig}
         * @constructor
         */
        public static get Default()
        {
            return this._Default || (this._Default = new this());
        }

        public get httpProxy(): string
        {
            return this._httpProxy;
        }

        public get httpsProxy(): string
        {
            return this._httpsProxy;
        }

        public get strictSSL(): string
        {
            return this._strictSSL;
        }

        protected constructor(
            protected _httpProxy  = ProxyDefaultConfig.get('proxy') || String(null),
            protected _httpsProxy = ProxyDefaultConfig.get('https-proxy') || String(null),
            protected _strictSSL  = ProxyDefaultConfig.get('strict-ssl') || String(null))
        {
            super();
        }

        public clone()
        {
            return new ProxyConfig(this._httpProxy, this._httpsProxy, this._strictSSL);
        }
    }

    /**
     *
     */
    export class ProxyConfig extends ProxyDefaultConfig
    {
        public set httpProxy(value: string)
        {
            if (value == null)
                ProxyConfig.rm('proxy');
            else
                ProxyConfig.set('proxy', this._httpProxy = value);
        }

        public set httpsProxy(value: string)
        {
            if (value == null)
                ProxyConfig.rm('https-proxy');
            else
                ProxyConfig.set('https-proxy', this._httpsProxy = value);
        }

        public set strictSSL(value: string)
        {
            if (value == null)
                ProxyConfig.rm('strict-ssl');
            else
                ProxyConfig.set('strict-ssl', this._strictSSL = value);
        }
    }
}

export default npm;
