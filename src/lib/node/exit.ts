import { bs } from 'bin/bs';
import chalk from 'chalk';
import lib from 'lib/core/lib';
import nodeCleanup from 'node-cleanup';
import { Nullable } from 'simplytyped';

namespace exit
{
    /**
     * Represents common system process signals.
     * @see https://nodejs.org/api/process.html#process_signal_events
     */
    export enum SignalTypes
    {
        /**
         * The SIGHUP signal is sent to a process when its controlling terminal is closed (hangup).
         */
        SIGHUP  = 1,
        /**
         * Terminal interrupt signal.
         * The SIGINT signal is sent to a process by its controlling terminal when a user wishes to interrupt the process.
         */
        SIGINT  = 2,
        /**
         * Terminal quit signal.
         * The SIGQUIT signal is sent to a process by its controlling terminal
         * when the user requests that the process quit and perform a core dump.
         */
        SIGQUIT = 3,
        /**
         * The SIGABRT and SIGIOT signal is sent to a process to tell it to abort, i.e. to terminate.
         * Used to signal fatal conditions in support libraries, situations where the current operation cannot be
         * completed but the main program can perform cleanup before exiting.
         * In contrast to SIGTERM and SIGINT, this signal cannot be ignored.
         */
        SIGABRT = 6,
        /**
         * Termination signal.
         * The SIGTERM signal is sent to a process to request its termination.
         * Unlike the SIGKILL signal, it can be caught and interpreted or ignored by the process.
         */
        SIGTERM = 15,
        /**
         * Kill (cannot be caught or ignored).
         * The SIGKILL signal is sent to a process to cause it to terminate immediately (kill).
         * In contrast to SIGTERM and SIGINT, this signal cannot be caught or ignored.
         * @access Use this enum member only for process termination in combination with process.kill.
         */
        SIGKILL = 9,
    }

    /**
     * @see http://www.tldp.org/LDP/abs/html/exitcodes.html
     */
    export enum ExitCodes
    {
        /**
         * This member ist really self explaining...
         */
        NormalExit           = 0,
        /**
         * Catchall for general errors.
         */
        Error                = 1,
        /**
         * Misuse of shell builtins (according to Bash documentation)
         */
        ShellBuiltinMisuse   = 2,
        /**
         * Command invoked cannot execute (not installed/permitted)
         */
        CommandFailed        = 126,
        /**
         * Command not found.
         */
        CommandNotFound      = 127,
        /**
         * Command not executable. Invalid argument to exit and should not be user-specified in a script.
         */
        CommandNotExecutable = 128,
        /**
         * Terminal has been closed or hangup.
         */
        TerminalClosed       = CommandNotExecutable + SignalTypes.SIGHUP,
        /**
         * Script terminated by Control-C
         */
        UserAbort            = CommandNotExecutable + SignalTypes.SIGINT,
        /**
         * Process shutdown.
         */
        UserQuit             = CommandNotExecutable + SignalTypes.SIGQUIT,
        /**
         * Process exit.
         */
        UserExit             = CommandNotExecutable + SignalTypes.SIGABRT,
        /**
         * Process termination request.
         */
        UserTerminate        = CommandNotExecutable + SignalTypes.SIGTERM,
        /**
         * Fatal error signal. (process killed)
         */
        FatalError           = CommandNotExecutable + SignalTypes.SIGKILL,
    }

    /**
     * Capture all node process environment exit events and create default exit codes.
     * @see https://www.gnu.org/software/libc/manual/html_node/Termination-Signals.html
     */
    nodeCleanup((exitCode: Nullable<number>, signal: Nullable<string>) =>
        {
            const logSignal = (_signal: string, code?: number) =>
            {
                if (code! > 0)
                    process.exitCode = code;
                lib.logMessage(chalk.bold.red(`<CLOSING> [${_signal}]`));
            };

            switch (signal) {
                // Process terminal closed
                case SignalTypes[SignalTypes.SIGHUP]:
                    logSignal(signal, ExitCodes.TerminalClosed);
                    break;
                // User exit through ctrl-C
                case SignalTypes[SignalTypes.SIGINT]:
                    logSignal(signal, ExitCodes.UserAbort);
                    break;
                // Application shut down
                case SignalTypes[SignalTypes.SIGQUIT]:
                    logSignal(signal, ExitCodes.UserQuit);
                    break;
                // Application exit
                case SignalTypes[SignalTypes.SIGABRT]:
                    logSignal(signal, ExitCodes.UserExit);
                    break;
                // User requests termination
                case SignalTypes[SignalTypes.SIGTERM]:
                    logSignal(signal, ExitCodes.UserTerminate);
                    break;
                // Normal exit
                case undefined:
                    if (bs.opts().verbose)
                        logSignal('EXIT');
                    break;
                // Ups, something has gone terribly wrong ...
                default:
                    logSignal('UNKNOWN SIGNAL');
                    break;
            }
            return true;
        },
        {
            ctrl_C           : '[ctrl-C]',
            uncaughtException: 'Uncaught exception occurred:',
        });

    /**
     * Register and handle node process closing events by using this function.
     * Use to release all resources before node exits.
     * @type {nodeCleanup}
     * @see https://www.npmjs.com/package/node-cleanup
     */
    export const RegisterExitHandler: typeof nodeCleanup = nodeCleanup;
}

export default exit;
