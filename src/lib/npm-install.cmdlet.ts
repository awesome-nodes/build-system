import CommandLet from 'lib/core/cmd';
import lib from 'lib/core/lib';
import npm from 'lib/node/npm-config';

namespace cmdlet
{
    /**
     * npm-install Command-Let
     * Provides the following dev-op must-haves:
     * - Installation of global node packages which can be defined by adding a
     *   globalDependencies* configuration section to the npm configuration file (package.json).
     * - Cached npm installs using a (gzip) compressed node_modules storage.
     * - Save option for temporary switching the npm registry during ``npm install``.
     * - Enable lightweight and fast CI builds by reducing the count and size of build artifacts.
     * - Gracefully closing of command-lets via signal handling.
     */
    export const npmInstallCommandLet = new class NPMInstallCommandLet extends CommandLet
    {
        public exec(packages?: string)
        {
            //region npm-cache arguments

            let command = `npm-cache install`;
            if (FORCE_CACHE_REFRESH)
                command += ' --forceRefresh';

            command += ` --cacheDirectory "${NPM_CACHE}"`;

            //endregion

            //region npm install arguments

            command += ` npm`;

            if (packages != null)
                command += ` ${packages}`;

            if (NPM_CACHE != npm.Config.get('cache'))
                command += ` --cache "${NPM_CACHE}"`;

            if (PRODUCTION)
                command += ' --production';

            if (IGNORE_SCRIPTS)
                command += ' --ignore-scripts';

            //endregion

            lib.exec(command);
        }
    }(
        'npm-install',
        'Installs globalDependencies section from package.json and invokes a npm install using a' +
        ' (gzip) compressed node_modules cache storage.',
        [
            {
                name        : 'cache',
                alias       : 'c',
                type        : String,
                typeLabel   : '{underline Dir}',
                description : 'Specify a different cache directory for storing node_modules caches.',
                defaultValue: npm.Config.get('cache'),

            },
            {
                name        : 'force-refresh',
                alias       : 'f',
                type        : Boolean,
                description : 'Force installing dependencies from package manager without cache.',
                defaultValue: false,
            },
            {
                name        : 'registry',
                alias       : 'r',
                type        : String,
                typeLabel   : '{underline URL}',
                description : 'The npm registry to be used.',
                defaultValue: npm.Config.get('registry'),
            },
            {
                name        : 'proxy',
                alias       : 'x',
                type        : Boolean,
                description : 'Use npm registry as proxy.',
                defaultValue: false,
            },
            {
                name        : 'weak-ssl',
                alias       : 'w',
                type        : Boolean,
                description : 'Disable SSL verification. I hope you know what you\'re doing . . .',
                defaultValue: false,
            },
            {
                name        : 'force-global',
                alias       : 'g',
                type        : Boolean,
                description : 'Force install of globalDependencies section of package.json.',
                defaultValue: false,
            },
            {
                name        : 'production',
                alias       : 'p',
                type        : Boolean,
                description : 'Install dependencies section of package.json.',
                defaultValue: false,
            },
            {
                name        : 'ignore-scripts',
                alias       : 'i',
                type        : Boolean,
                description : 'Skip npm run scripts.',
                defaultValue: false,
            },
            {
                name       : 'help',
                alias      : 'h',
                type       : Boolean,
                description: 'Print this usage guide.',
            },
        ],
    );

    // Initialize command line processor
    export const options = npmInstallCommandLet.args;

    // Read command line parameters
    export const STRICT_SSL: string           = String(!options.weakSsl);
    export const NPM_CACHE: string            = options.cache;
    export const FORCE_CACHE_REFRESH: boolean = options.forceRefresh;
    export const NPM_REGISTRY: string         = options.registry;
    export const USE_PROXY: boolean           = options.proxy;
    export const HTTP_PROXY: string           = NPM_REGISTRY;
    export const HTTPS_PROXY: string          = NPM_REGISTRY;
    export const FORCE_GLOBAL: boolean        = options.forceGlobal;
    export const PRODUCTION: boolean          = options.production;
    export const IGNORE_SCRIPTS: boolean      = options.ignoreScripts;
}

export default cmdlet;
