import chalk from 'chalk';
import { OptionValues } from 'commander';
import lib from 'lib/core/lib';
import cmdlet from 'lib/demo.cmdlet';
import exit from 'lib/node/exit';
import { Nullable } from 'simplytyped';
import options = cmdlet.options;
import PRODUCTION = cmdlet.PRODUCTION;
import SignalTypes = exit.SignalTypes;

/**
 * Main Entry Point
 */
export default function(programOptions: OptionValues)
{
    if (programOptions.help || options.help) {
        console.log(cmdlet.buildSystemDemoCommandLet.usage);
        process.exit(0);
    }

    // Make the command-let gracefully respond to shutdown signals
    exit.RegisterExitHandler((exitCode: Nullable<number>, signal: Nullable<string>) =>
    {
        switch (signal) {
            // Process terminal closed
            case SignalTypes[SignalTypes.SIGHUP]:
            // User exit through ctrl-C
            case SignalTypes[SignalTypes.SIGINT]:
            // Application shut down
            case SignalTypes[SignalTypes.SIGQUIT]:
            // Application exit
            case SignalTypes[SignalTypes.SIGABRT]:
            // User requests termination
            case SignalTypes[SignalTypes.SIGTERM]:
            case null:
                // exit gracefully and revert unfinished work.
                break;
            default:
                // Node process is in exit state, we have to exit immediately.
                return true;
        }
        // Do cleanup ...
        return true;
    });

    // Register custom log handler
    lib.util.logFormatHandler = (type: string) =>
    {
        return `[DEMO] [${type}]`;
    };

    try {
        if (PRODUCTION)
            lib.logMessage(chalk.green.bold.underline('You are awesome!'));
        else {
            lib.logError('You should install BuildSystem globally only!');
            process.exit(exit.ExitCodes.FatalError);
        }

        // Awesomeness
        cmdlet.buildSystemDemoCommandLet.exec();
    }
    finally {
        // Do cleanup ...
    }
}
