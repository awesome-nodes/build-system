import { OptionValues } from 'commander';
import cmdlet from 'lib/bundle/webpack.cmdlet';
import webpackConfig from 'lib/bundle/webpack.config';
import lib from 'lib/core/lib';
import exit from 'lib/node/exit';
import { Nullable } from 'simplytyped';
import webpack, { MultiStats, Stats } from 'webpack';
import SignalTypes = exit.SignalTypes;

/**
 * Main Entry Point
 * @param {OptionValues} programOptions
 */
export default async (programOptions: OptionValues) =>
{
    if (programOptions.help || cmdlet.options.help) {
        console.log(cmdlet.webpackCommandlet.usage);
        process.exit(0);
    }

    // Make the command-let gracefully respond to shutdown signals
    exit.RegisterExitHandler((exitCode: Nullable<number>, signal: Nullable<string>) =>
    {
        switch (signal) {
            // Process terminal closed
            case SignalTypes[SignalTypes.SIGHUP]:
            // User exit through ctrl-C
            case SignalTypes[SignalTypes.SIGINT]:
            // Application shut down
            case SignalTypes[SignalTypes.SIGQUIT]:
            // Application exit
            case SignalTypes[SignalTypes.SIGABRT]:
            // User requests termination
            case SignalTypes[SignalTypes.SIGTERM]:
            case null:
                // exit gracefully.
                lib.exec(cmdlet.clean);
                break;
            default:
                // Node process is in exit state, we have to exit immediately.
                return true;
        }
        // Do cleanup ...
        return true;
    });

    let compiler: webpack.Compiler | webpack.MultiCompiler;
    const env = process.env.NODE_ENV || 'production';
    compiler = webpack(webpackConfig(env) as webpack.Configuration[]);

    return new Promise((resolve, reject) =>
    {
        setTimeout(
            () =>
            {
                const handler = (err?: Error, stats?: Stats | MultiStats) =>
                {
                    if (err) {
                        console.error(err);
                        process.exitCode = exit.ExitCodes.ShellBuiltinMisuse;
                        reject(err);
                    } else if (stats) {
                        lib.logMessage(stats.toString());
                        if (!cmdlet.WATCH)
                            resolve(stats);
                    } else
                        lib.logWarning('WebPack callback stats empty!');
                };

                if (cmdlet.WATCH)
                    compiler.watch({}, handler);
                else
                    compiler.run(handler);
            },
            0,
        );
    });
};
