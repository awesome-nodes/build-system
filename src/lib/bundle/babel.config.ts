require('../../tsconfig');
import { ConfigAPI, TransformOptions } from 'babel__core';
import TypeScript from 'lib/typescript/config';
import process from 'process';

//tslint:disable-next-line:only-arrow-functions
export default function babelConfig(api?: ConfigAPI)
{
    api && api.cache.forever();

    const modules = (process.env.CJS === 'true')
        ? 'commonjs'
        : false;
    const targets = modules !== false
        ? {node: true}
        : {browsers: 'last 5 Chrome versions'};

    return {
        comments: false,
        presets : [
            [
                '@babel/env',
                {
                    modules,
                    useBuiltIns: 'usage',
                    corejs     : '3.19.2',
                    targets,
                },
            ],
            [
                '@babel/typescript',
                {
                    allowNamespaces: true,

                },
            ],

        ],
        plugins : [
            ['@babel/plugin-proposal-decorators', { legacy: true }],
            ['@babel/plugin-proposal-class-properties', { loose: true }],
            ['@babel/plugin-proposal-private-methods', { loose: true }],
            ['@babel/plugin-proposal-private-property-in-object', { loose: true }],
            '@babel/plugin-proposal-numeric-separator',
            '@babel/proposal-object-rest-spread',
            '@babel/plugin-syntax-bigint', [
                'module-resolver',
                {
                    root      : ['./src'],
                    alias     : TypeScript.Config.File.CreateAliasConfig(),
                    extensions: ['.js', '.ts'],
                },
            ],
        ],
    } as TransformOptions;
}
