import CommandLet from 'lib/core/cmd';

namespace cmdlet
{
    /**
     * Webpack Command-Let
     * Provides the following:
     * - Explains on how to write a awesome command-let with BuildSystem.
     */
    export const webpackCommandlet = new class WebpackCommandLet extends CommandLet
    {
    }(
        'bundle-webpack',
        `Builds typescript with webpack as UMD bundle located at the 'dist' directory.`,
        [
            {
                name       : 'raw',
                alias      : 'r',
                type       : String,
                multiple   : true,
                description: 'A list of file extensions which should be treated as raw text by the loader.',
                typeLabel  : '{underline raw-loader} ...',
            },
            {
                name        : 'watch',
                alias       : 'w',
                type        : Boolean,
                description : 'Enables automatic recompile on file changes.',
                defaultValue: false,
            },
            {
                name       : 'help',
                alias      : 'h',
                type       : Boolean,
                description: 'Print this usage guide.',
            },
        ],
    );

    export const clean = `rimraf dist`;

    // Initialize command line processor
    export const options = webpackCommandlet.args;

    // Read command line parameters
    export const RAW: string[] = options.raw;

    // Read command line parameters
    export const WATCH: boolean = options.watch;
}

export default cmdlet;
