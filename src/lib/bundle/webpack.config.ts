require('../../tsconfig');
import glob from 'glob';
import babelConfig from 'lib/bundle/babel.config';
import cmdlet from 'lib/bundle/webpack.cmdlet';
import lib from 'lib/core/lib';
import TypeScript from 'lib/typescript/config';
import path from 'path';
import { PlainObject } from 'simplytyped';
import { Configuration } from 'webpack';

const srcEntry: PlainObject = {};

// Use the package name for the default entry point if there is any.
glob.sync('./src/index.ts').forEach(file =>
{
    srcEntry[lib.util.packageName] = file;
});

// Lookup more entry points located at the package root scope.
glob.sync(
    '*.ts',
    {
        ignore: ['index.ts', '*.spec.ts'],
        cwd   : path.resolve(process.cwd(), './src'),
    }).forEach(file =>
{
    srcEntry[path.basename(file).replace(/\.[^/.]+$/, '')] = `./src/${file}`;
});

const loaders = [];

if (cmdlet.RAW && cmdlet.RAW.length > 0)
    loaders.push({
        test: new RegExp(`\.(${cmdlet.RAW.join('|')})$`, 'i'),
        use : 'raw-loader',
    });

loaders.push({
        test   : /\.(js|jsx|tsx|ts)$/,
        exclude: [
            path.resolve(process.cwd(), 'dist'),
            path.resolve(process.cwd(), 'src/**/*.spec.ts'),
        ],
        use    : [
            {
                loader : 'babel-loader',
                options: babelConfig(),
            },
            {
                loader: 'remove-comments-loader',
            },
        ],
    },
);

const devConfig = {
    mode     : 'development',
    target   : 'node',
    externals: [require('webpack-node-externals')()],
    entry    : srcEntry,
    // devtool: 'inline-source-map',
    devtool: 'source-map',
    plugins: [
        new (require('webpack-messages'))({
            name  : lib.util.packageName,
            logger: (str: string) => console.log(`>> ${str}`),
        }),
    ],
    module : {
        rules: loaders,
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', 'jsx', '.json'],
        modules   : [
            path.resolve(process.cwd(), 'src/'),
        ],
        alias     : TypeScript.Config.File.CreateAliasConfig(true),
    },
    output : {
        path          : path.resolve(process.cwd(), 'dist'),
        filename      : '[name].js',
        libraryTarget : 'umd',
        library       : lib.util.packageName,
        umdNamedDefine: true,
    },
} as Configuration;

const prodConfig = {
    ...devConfig,
    mode        : 'production',
    optimization: {
        minimize: true,
    },
    output      : {
        ...devConfig.output,
        filename: '[name].min.js',
    },
} as Configuration;

export default function webpackConfig(env: string)
{
    switch (env || process.env.NODE_ENV || 'production') {
        case 'production':
            return [devConfig, prodConfig];
        case'development':
        default:
            return devConfig;
    }
}
