import { OptionValues } from 'commander';
import cmdlet from 'lib/bundle/babel.cmdlet';
import lib from 'lib/core/lib';
import exit from 'lib/node/exit';
import { Nullable } from 'simplytyped';
import SignalTypes = exit.SignalTypes;

/**
 * Main Entry Point
 * @param {OptionValues} programOptions
 */
export default (programOptions: OptionValues) =>
{
    if (programOptions.help || cmdlet.options.help) {
        console.log(cmdlet.babelCommandLet.usage);
        process.exit(0);
    }

    // Make the command-let gracefully respond to shutdown signals
    exit.RegisterExitHandler((exitCode: Nullable<number>, signal: Nullable<string>) =>
    {
        switch (signal) {
            // Process terminal closed
            case SignalTypes[SignalTypes.SIGHUP]:
            // User exit through ctrl-C
            case SignalTypes[SignalTypes.SIGINT]:
            // Application shut down
            case SignalTypes[SignalTypes.SIGQUIT]:
            // Application exit
            case SignalTypes[SignalTypes.SIGABRT]:
            // User requests termination
            case SignalTypes[SignalTypes.SIGTERM]:
            case null:
                // exit gracefully.
                lib.exec(cmdlet.clean);
                break;
            default:
                // Node process is in exit state, we have to exit immediately.
                return true;
        }
        // Do cleanup ...
        return true;
    });

    try {
        // Awesomeness
        cmdlet.babelCommandLet.exec();
    }
    finally {
        // Do cleanup ...
    }
};
