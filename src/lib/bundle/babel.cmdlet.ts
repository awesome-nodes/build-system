import CommandLet from 'lib/core/cmd';
import lib from 'lib/core/lib';
import path from 'path';

namespace cmdlet
{
    /**
     * Babel Command-Let
     * Provides the following:
     * - Explains on how to write a awesome command-let with BuildSystem.
     */
    export const babelCommandLet = new class BabelCommandLet extends CommandLet
    {
        public exec()
        {
            const ttypescript   = `npx ttsc --emitDeclarationOnly --project tsconfig.lib.json`;
            const babelOptions  = `--extensions ".ts,.tsx" --source-maps inline ${path.resolve(process.cwd(), 'tsconfig.lib.json')}`;
            const babelCmd      = `babel ./src --config-file ${path.resolve(__dirname, 'babel.config.js')} --out-dir`;
            let cjsCommand      = `${ttypescript}`;
            let cjsBabelCommand = `npx cross-env CJS=true ${babelCmd} ./lib ${babelOptions}`;
            let esmCommand      = `${ttypescript} --outDir ./lib-esm`;
            let esmBabelCommand = `npx ${babelCmd} ./lib-esm ${babelOptions}`;
            if (WATCH) {
                cjsCommand += ' --watch';
                cjsBabelCommand += ' --watch';
                esmCommand += ' --watch';
                esmBabelCommand += ' --watch';
            }

            if (BUILD_ALL) {
                lib.exec(clean);
                lib.exec(`concurrently \"${cjsCommand}\" \"${cjsBabelCommand}\" \"${esmCommand}\" \"${esmBabelCommand}\"`);
            } else {
                lib.exec(clean);
                lib.exec(CJS ? `concurrently \"${cjsCommand}\" \"${cjsBabelCommand}\"` : `concurrently \"${esmCommand}\" \"${esmBabelCommand}\"`);
            }
        }
    }(
        'bundle-babel',
        `Builds typescript with babel by default as ES6 module located at the 'lib-esm' directory.`,
        [
            {
                name        : 'commonjs',
                alias       : 'c',
                type        : Boolean,
                description : `Builds a commonJS package located at the 'lib' directory.`,
                defaultValue: false,
            },
            {
                name        : 'buildAll',
                alias       : 'a',
                type        : Boolean,
                description : 'Builds both commonJS and ES6 packages.',
                defaultValue: false,
            },
            {
                name        : 'watch',
                alias       : 'w',
                type        : Boolean,
                description : 'Enables automatic recompile on file changes.',
                defaultValue: false,
            },
            {
                name       : 'help',
                alias      : 'h',
                type       : Boolean,
                description: 'Print this usage guide.',
            },
        ],
    );

    export const clean = `concurrently \"rimraf lib\" \"rimraf lib-esm\"`;

    // Initialize command line processor
    export const options = babelCommandLet.args;

    // Read command line parameters
    export const CJS: boolean       = options.commonjs;
    export const BUILD_ALL: boolean = options.buildAll;
    export const WATCH: boolean     = options.watch;
}

export default cmdlet;
