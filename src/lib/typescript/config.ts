import * as fs from 'fs';
import hjson from 'hjson';
import nodePath from 'path';
import { PlainObject } from 'simplytyped';
import ts from 'typescript/lib/tsserverlibrary';

namespace TypeScript
{
    export type ConfigTypes = 'config' | 'node' | 'paths' | 'types' | 'lib' | 'bundle';
    /**
     * Represents all possible tsconfig options.
     * Note: This type is based on all possible grunt-ts options because the ITaskOptions interface contains also
     * settings from the tsconfig.compilerOptions section and the same vice versa.
     */
    export type IConfigOptions = ts.server.ProjectInfoTelemetryEventData;

    export class Config
    {
        /**
         * Returns the basic typescript configuration (root) scope which contains basic compilation settings:
         * - Output Style
         * - ECMAScript library
         * - Type System Strictness
         * @returns {TypeScript.Config}
         */
        public static get File()
        {
            return new this('config');
        }

        /**
         * Returns the nodejs typescript configuration to be used for compilation.
         * @returns {TypeScript.Config}
         */
        public static get Node()
        {
            return new this('node');
        }

        /**
         * Returns the typescript configuration responsible for glob, paths and files to be used for compilation.
         * @returns {TypeScript.Config}
         */
        public static get Paths()
        {
            return new this('paths');
        }

        /**
         * Returns the typescript configuration responsible for type root specification as well type library spec.
         * Note: This setting enables full control over loading of node_modules/@types contents (IDE & Build).
         * (enables parallel nodeJS development possible through IDE-build with IDEA using
         * --target ES5 --lib "es2016" --project tsconfig.node.json)
         * @returns {TypeScript.Config}
         */
        public static get Types()
        {
            return new this('types');
        }

        /**
         * Initializes this typescript configuration instance.
         * @param {TypeScript.ConfigTypes} ConfigType
         * @param {IConfigOptions} File
         * @constructor
         */
        constructor(public readonly ConfigType: ConfigTypes = 'config',
                    public readonly File: IConfigOptions    = hjson.parse(fs.readFileSync(
                        nodePath.resolve(process.cwd(), Config.FilePath(ConfigType)), 'utf-8')))
        {
        }

        /**
         * Returns the compiler options section of the current tsconfig file.
         * @returns {ts.CompilerOptions}
         */
        public get CompilerOptions()
        {
            return this.File.compilerOptions;
        }

        /**
         * Utillity function for config path generation.
         * @param {TypeScript.ConfigTypes} configType
         * @returns {string}
         * @constructor
         */
        public static FilePath(configType: ConfigTypes = 'config')
        {
            return `${process.cwd()}/ts${configType == 'config' ? configType : `config.${configType}`}.json`;
        }

        /**
         * Returns a babel an webpack compatible alias configuration.
         * @returns {string[]}
         */
        public CreateAliasConfig(resolve = false): PlainObject
        {
            const aliasConfig: PlainObject = {};
            const paths                    = (this.CompilerOptions.paths || []) as string[];
            for (const _key in paths)
                if (_key && /[\w-]+/.test(_key)) {
                    let path = (paths[_key] as unknown as string[]).shift() as string;
                    if (!path.startsWith('./'))
                        path = `./${path}`;
                    aliasConfig[_key] = resolve ? nodePath.resolve(process.cwd(), path) : path;
                }

            return aliasConfig;
        }
    }
}

export default TypeScript;
