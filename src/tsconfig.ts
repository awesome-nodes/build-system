require('ts-node/register');

//eslint-disable-next-line @typescript-eslint/no-var-requires
const tsconfigPaths      = require('tsconfig-paths');
const unregisterTsConfig = tsconfigPaths.register({
    baseUrl: __dirname, paths: {},
});

export default unregisterTsConfig;
