#!/usr/bin/env node
require('../tsconfig');
import chalk from 'chalk';
import { OptionDefinition } from 'command-line-usage';
import { Command, OptionValues, program } from 'commander';
import glob from 'glob';
import CommandLet from 'lib/core/cmd';
import lib from 'lib/core/lib';
import exit from 'lib/node/exit';
import * as sh from 'shelljs';

export const bs: typeof program = program;
const taskDir                   = `${__dirname}/../lib/`;

// Initialize program 'commander'
program
    .name(lib.util.packageJSON.name as string)
    .version(lib.util.packageJSON.version || 'unknown')
    .option('-tc, --team-city', 'Enable TeamCity compatible logging', false)
    .option('-v, --verbose', 'Verbose output', false)
    .description(`BuildSystem: ${lib.util.packageJSON.description}`).addHelpCommand(true)
    .allowUnknownOption();

// This user really needs help
if (process.argv.length < 3) {
    lib.logError('Missing argument (task name)');
    program.help();
}

// Prepare main action
//tslint:disable-next-line:only-arrow-functions
const commandAction = async function(params: OptionValues, options: OptionValues, cmdlet: Command)
{
    const taskName     = cmdlet.name();
    const taskFileName = taskName.replace('-', '/');
    const taskPath     = taskDir + taskFileName;
    const scriptExists = sh.test('-f', `${taskPath}.js`);

    // Set the program name
    // (BuildSystem takes identity of the selected task library.)
    if (scriptExists)
        program.name(taskName);

    // Break out with message if script not found
    if (!scriptExists) {
        let failedCmdlet = taskPath;
        if (!taskFileName.match(/\.js/))
            failedCmdlet += '.js';

        lib.logError(`BuildSystem script 'lib/%s' not found!`, chalk.bold(failedCmdlet));
        sh.exit(exit.ExitCodes.CommandNotFound);
    }

    // Run BuildSystem script and handle errors.
    try {
        (await import(taskPath)).default(options);
    }
    catch (ex) {
        lib.logError(chalk.underline.bold.red(`Unhandled error during command-let execution occurred.`));
        if (lib.util.isNodeError(ex))
            console.error(ex);
        else {
            const lines = String(ex).split('\n');
            if (lines.length > 1)
                lib.logError(
                    '%s\n%s',
                    chalk.underline.bold.red(lines[0]),
                    chalk.redBright(lines.slice(1).join('\n')),
                );
            else
                lib.logError(chalk.bold.red(lines[0]));
        }
        const exitCode = process.exitCode || exit.ExitCodes.Error;
        lib.logError(
            chalk.underline.bold.red(`BuildSystem script '%s' failed! Exit Code: '%s'`),
            taskName,
            exitCode.toString(),
        );
        sh.exit(exitCode);
    }

    lib.logMessage(chalk.underline.bold.green(`BuildSystem script '%s' finished!`), taskName);
}
    // Bind the task action callback function to the program scope
    // (Enables logging origin detection for log functions provided by lib/util)
    .bind(program);

// Read all available cmdlets and add them as commands
const cmdLets = glob.sync(`${taskDir}**/*.cmdlet.js`, {realpath: true});
for (const _cmdLet of cmdLets) {
    const cmdletNamespace = require(_cmdLet).default;
    for (const _cmdletType in cmdletNamespace) {
        if (!cmdletNamespace.hasOwnProperty(_cmdletType))
            continue;
        if (cmdletNamespace[_cmdletType] instanceof CommandLet) {
            const cmdletInstance = cmdletNamespace[_cmdletType] as CommandLet;
            const command        = program.command(`${cmdletInstance.header} [params...]`, {});
            for (const _option of cmdletInstance.options) {
                command.option(
                    `-${_option.alias}, --${_option.name}`,
                    (_option as OptionDefinition).description,
                    _option.defaultValue,
                );
                command.action(commandAction);
            }
        }
    }
}

// Run BuildSystem
program.parse(process.argv);
