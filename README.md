# Awesome Nodes Zero Configuration Build System
> Integrated Zero Configuration Build System for Awesome Nodes Apps and Framework Components

## Project Goals

### Zero Configuration Build

The main target of Build System is to provide out of the box building of TypeScript libraries as:

* ES5 for NodeJS applications
* ES6 for tree shaking builds
* and as UMD bundle for web applications

### Command-Let Development

The secondary target of Build System is to provide out of the box command-let development with exit code handling.
Command-let development provides a neat way for writing business logic in complex continuous integration scenarios.

#### Exit Code Handling

Exit code handling ensures intercommunication of execution status between command-lets and execution host.
> There is no chance to exit not gracefully when using Command-Let`s written with Build System.

See `src/lib/demo.cmdlet.ts` and `src/lib/demo.ts` for a quick demonstration.

## Installation

BuildSystem can be installed globally

```sh
$ npm run start
```

or

```sh
$ npm install -g @awesome-nodes/build-system
```

## Usage

## CLI

### Run Builds-System locally

```sh
$ node bin/bs -h
```

### Run Builds-System *globally installed* or as *project dependency*

```sh
$ bs -h
```

### Help Output

```text
Options:
  -V, --version                         output the version number
  -tc, --team-city                      Enable TeamCity compatible logging (default: false)
  -v, --verbose                         Verbose output (default: false)
  -h, --help                            display help for command

Commands:
  bundle-babel [options] [params...]
  bundle-webpack [options] [params...]
  demo [options] [params...]
  npm-install [options] [params...]
  help [command]                        display help for command
```
### Setup
1. Install Build System within your node library project as development dependency
   ```bash
   $ npm install --save-dev @awesome-nodes/build-system
   ```
2. Add build scripts to your node package configuration
   ```json
   {
     "scripts": {
       "build": "concurrently \"npm run build:es\" \"npm run build:umd\"",
       "build:es": "bs bundle-babel -a",
       "build:umd": "bs bundle-webpack",
       "build:watch": "cross-env NODE_ENV=development concurrently \"npm run build:es -- -w\" \"npm run build:umd -- -w\""
      }
   }
   ```

# Build System Node Module

## Directory Layout

* `bin` - Build System command line binary  
* `lib` - Build System library
* `src` - Build System sources

## Node Module Build

### Usage of [NPM] instead of [Yarn]
Zero configuration requires to be non dependent on any dependency for initial build host provisioning.
So we relay on the `npm` node package manager for any kind of node module specific operations.

[NPM]: <https://www.npmjs.com/>
[Yarn]: <https://yarnpkg.com>
